plex role
=========

Setup a Plex Media Server.

Requirements
------------

This role assumes the following:

  - User called `ansible` is present on the system
  - User `ansible` can use sudo without password
  - User `ansible` uses `/bin/sh` compatible shell

Role Variables
--------------

- `nvidia`: Whether or not to install NVIDIA GPU acceleration support, default: no

Dependencies
------------

- ansible.posix >= 1.3.0

Example Playbook
----------------


```yaml
# inventory.yml
---
all:
  hosts:
    server1:
      nvidia: yes
    server2:
```

```yaml
# playbook.yml
---
- hosts: all
  vars:
    ssh_keys:
      - "ssh-ed25519 AAA..."
      - "ssh-rsa AAA..."
    dns_servers:
      - "1.1.1.1"
      - "2606:4700:4700::1111"
  roles:
     - plex
```

License
-------

MIT

Author Information
------------------

  - Ricardo (XenGi) Band <email@ricardo.band>

